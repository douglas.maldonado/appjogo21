package br.com.itau;

import java.util.*;

public class Baralho {
    private Map<String, Integer> baralho = new HashMap<String, Integer>();
//    Random random = new Random();
//    String randomkey;
//    Integer valor;

    public Map<String, Integer> getBaralho() {

        for(Carta c : Carta.values()){
            for(Naipe n : Naipe.values()){
                baralho.put(String.valueOf(c)+String.valueOf(n), c.getValor());
            }
        }

        return baralho;
    }

//    public String getRandomkey() {
//        List<String> keys = new ArrayList<String>(baralho.keySet());
//        randomkey = keys.get(random.nextInt(keys.size()));
//
//        keys.remove(randomkey);
//        return randomkey;
//    }

}
