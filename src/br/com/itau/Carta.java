package br.com.itau;

public enum Carta {
    A(1),
    DOIS(2),
    TRÊS(3),
    QUATRO(4),
    CINCO(5),
    SEIS(6),
    SETE(7),
    OITO(8),
    NOVE(9),
    DEZ(10),
    J(10),
    Q(10),
    K(10);

    int valor;

    public int getValor() {
        return valor;
    }

    Carta(int valor) {
        this.valor = valor;
    }
}
