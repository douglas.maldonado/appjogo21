package br.com.itau;

public enum Naipe {
    Espada("espadas"),
    Paus("paus"),
    Ouro("ouro"),
    Copas("copas");

    String naipe;

    Naipe(String naipe) {
        this.naipe = naipe;
    }

}

