package br.com.itau;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Banca {
    private Baralho baralho;
    Random random = new Random();
    String randomkey;
    Integer valor;
    List<String> keys = new ArrayList<String>();
    String continuar = "S";
    int valorSoma = 0;
    Scanner scanner = new Scanner(System.in);

    public Banca(Baralho baralho) {
        this.baralho = baralho;
    }

    public void iniciarJogo(){
        keys.addAll(baralho.getBaralho().keySet());

        System.out.println("Inicio BlackJack");
        System.out.println("Distribuindo Cartas");

        for(int i = 0; i < 2; i++){
            randomkey = keys.get(random.nextInt(keys.size()));
            valor = baralho.getBaralho().get(randomkey);

            valorSoma += valor;

            System.out.print(randomkey + " ");

            keys.remove(randomkey);

        }

        System.out.println(valorSoma);

    }

    public void pedirCarta(){
        System.out.println("Deseja Continuar(S/N)");
        continuar = scanner.nextLine();

        while (continuar.equalsIgnoreCase("S")){
            randomkey = keys.get(random.nextInt(keys.size()));
            valor = baralho.getBaralho().get(randomkey);

            System.out.println(randomkey);

            valorSoma += valor;
            System.out.println(valorSoma);

            keys.remove(randomkey);

            System.out.println("Deseja Continuar? (S/N)");
            continuar = scanner.nextLine();
        }
    }
}
